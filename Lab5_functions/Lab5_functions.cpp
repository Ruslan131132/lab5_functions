﻿

#include <iostream>
using namespace std;
void func(int a[5][5], int n) {
	int sumdiag;
	int sumdiag1 = 0;
	int sumdiag2 = 0;
	for (int i = 0; i < 5; i += 1) {
		for (int j = 0; j < 5; j += 1) {
			if (i + j == 4) {
				sumdiag2 += a[i][j];
			}
			if (i == j) {
				sumdiag1 += a[i][j];
			}
		}
	}
	sumdiag = sumdiag1 + sumdiag2;
	cout << "Sum of diagonals : " << sumdiag << endl;
}
int func2(int n) {
	int sumcif = 0;
	while (n > 0) {
		sumcif += (n % 10);
		n /= 10;
	}
	return sumcif;
}
int main()
{
	cout << "Hello World! Let's do this, NOW!!" << endl;
	cout << endl;
	cout << "Task Number 1 \n" << endl;
	int arr[5][5];
	for (int i = 0; i < 5; i += 1) {
		for (int j = 0; j < 5; j += 1) {
			if (i + j == 4) {
				arr[i][j] = 25;
				cout << arr[i][j] << '\t';
			}
			if (i + j < 4) {
				arr[i][j] = 1;
				cout << arr[i][j] << '\t';
			}
			if (i + j > 4) {
				arr[i][j] = 2;
				cout << arr[i][j] << '\t';
			}
		}
		cout << endl;
	}
	cout << endl;
	func(arr, 25);
	cout << endl;
	cout << "Task Number 2\n " << endl;
	int arr2[5];
	int vsper;
	for (int i = 0; i < 5; i += 1) {
		cout << "Element of array with index " << i << " is ";
		cin >> arr2[i];
	}
	for (int k = 0; k < 4; k++) {
		for (int j = 0; j < 4; j++) {
			if (func2(arr2[j]) < func2(arr2[j + 1])) {
				vsper = arr2[j];
				arr2[j] = arr2[j + 1];
				arr2[j + 1] = vsper;
			}
		}
	}
	cout << "changed array: ";
	for (int i = 0; i < 5; i++) {
		cout << arr2[i] << "\t";
	}
	getchar();
	cin.get();
	return 0;
}
